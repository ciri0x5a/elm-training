# elm-training

elm sandbox

real-time compile:
`elm reactor`

update elm globally (remove elm-stuff dir and stop reactor):
`npm install -g elm@latest-$VERSION`

intall/update deps:
`elm install elm/json`