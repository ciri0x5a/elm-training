module FreshStart exposing (..)

import Html exposing (text)

-- ALIASES
type alias Pracownik =
    { name : String
    , job: String
    }

ziom : Pracownik
ziom =
    { name = "Seba"
    , job = "pijak"
    }

jestPijakiem : Pracownik -> Bool
jestPijakiem {job} =
    job == "pijak"

witamy : Pracownik -> String
witamy pracownik =
    if jestPijakiem pracownik then
        " pijaku"
    else
        " zdrowy, mlody czlowiek"


-- SWITCHES
type Action
    = Run String
    | Walk

myAction : Action -> String
myAction action =
    case action of
        Walk -> "Slow"

        Run speed -> speed

-- LISTS (:: operator)
myList = ["a", "b", " ostatni z arrayki"]
getLast : List String -> String
getLast list =
    case list of
        [last] -> last
        [] -> "pusto"
        _ :: rest ->
            getLast rest

main = "Hello" ++ witamy ziom ++ " biegniesz tak: " ++ myAction (Run "123") ++ (getLast myList) |> text