module HttpPlayground exposing (..)

import Browser
import Html exposing (Html, text, pre)
import Http exposing (Error(..))

-- MAIN
main = Browser.element
    { init = init
    , update = update
    , subscriptions = subscriptions
    , view = view
    }


-- MODEL
type Model
    = Failure String
    | Loading
    | Success String


init : () -> (Model, Cmd Msg)
init _ =
    ( Loading
    , Http.get
        { url = "http://www.gutenberg.org/cache/epub/6456/pg6456.txt" --https://cors-anywhere.herokuapp.com/
        , expect = Http.expectString GotText
        }
    )


-- UPDATE
type Msg
    = GotText (Result Http.Error String)


update : Msg -> Model -> (Model, Cmd Msg)
update msg _ =
    case msg of
        GotText result ->
            case result of
                Ok fullText ->
                    (Success fullText, Cmd.none)
                Err errorMsg ->
                    (Failure <| errorToString errorMsg, Cmd.none)


-- SUBSCRIPTIONS
subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


-- VIEW
view : Model -> Html Msg
view model =
    case model of
        Failure msg ->
            text msg
        Loading ->
            text "Loading..."
        Success fullText ->
            pre [] [ text fullText ]

errorToString : Http.Error -> String
errorToString err =
    case err of
        Timeout ->
            "Timeout exceeded"

        NetworkError ->
            "Network error (try workaround with cross-site-request in comment)"

        BadStatus resp ->
            "Error with status: " ++ String.fromInt resp

        BadBody text ->
            "Unexpected response from api: " ++ text

        BadUrl url ->
            "Malformed url: " ++ url